EL=$(wildcard *.el)
ELC=${EL:%.el=%.elc}

GENFILE=.cask-install ${ELC}
GENFOLDER=.cask dist

CLEANTASKS=${GENFILE:%=%-clean} ${GENFOLDER:%=%-clean-r}

COMMANDS=troubleshoot

all: ${ELC}

${COMMANDS}: %: ${ELC}
	cask emacs -l emacsism -e emacsism-$@

clean: ${CLEANTASKS}

rebuild: clean all

${ELC}: %.elc: %.el .cask-install
	cask emacs -batch -f batch-byte-compile $<

.cask-install: Cask
	cask install --verbose
	touch $@

%-clean:
	rm -f $*

%-clean-r:
	rm -rf $*
