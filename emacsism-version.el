;;; emacsism-version --- Versioninfo of emacs-exercism-client

;;; Commentary:

;;; Code:

(defconst emacsism-version "0.0.1-pre"
  "Version of emacsism.")

(provide 'emacsism-version)
;;; emacsism-version.el ends here
