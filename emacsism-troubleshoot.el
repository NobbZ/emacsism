;;; emacsism-troubleshoot --- Troubleshooting emacsism configuration

;;; Commentary:

;;; Code:

(require 'request)
(require 'widget)
(require 'emacsism-config)
(require 'emacsism-version)

(defconst emacsism/troubleshoot-buffer-name "*Emacsism Troubleshoot*"
  "The name of the buffer used to display emacsism troubleshooting information.")

(defvar emacsism/troubleshoot-github-info nil)
(defvar emacsism/troubleshoot-exercism-info nil)

;;;###autoload
(defun emacsism-troubleshoot ()
  "Display information which helps troubleshooting emacsism."
  (interactive)
  (let ((conf (emacsism/read-config)))
    (switch-to-buffer-other-window
     (get-buffer-create emacsism/troubleshoot-buffer-name))
    (emacsism-troubleshoot-mode)
    (emacsism/troubleshoot-update-display)
    (let ((inhibit-read-only t))
      (emacsism/troubleshoot-collect-info))))

(define-derived-mode emacsism-troubleshoot-mode special-mode "Emacsism-Troubleshoot"
  "Display some troubleshooting information.")

(defun emacsism/troubleshoot-collect-info ()
  "Collects data from the APIs."
  (request
   (concat (alist-get :base-url emacsism-config) "/ping")
   :parser 'json-read
   :success (cl-function
             (lambda (&key data &allow-other-keys)
               (switch-to-buffer emacsism/troubleshoot-buffer-name)
               (setq emacsism/troubleshoot-exercism-info '((:status . "[connected]") (:timing . "Not yet implemented")))
               (let ((inhibit-read-only t))
                 (emacsism/troubleshoot-update-display))))
   :error (cl-function
           (lambda (&key error &allow-other-keys)
             (switch-to-buffer emacsism/troubleshoot-buffer-name)
             (setq emacsism/troubleshoot-exercism-info `((:status . (format "%s" ,error)) (:timing . "Not yet implemented")))
             (let ((inhibit-read-only t))
               (emacsism/troubleshoot-update-display))))))

(defun emacsism/mangle-token (token)
  "Mangle the given TOKEN by replacing everything but the first and last 4 characters by asterisks."
  (let* ((rep (- (length token) 8))
         (head (s-left 4 token))
         (tail (s-right 4 token))
         (mid (s-repeat rep "*")))
    (s-concat head mid tail)))

(defun emacsism/troubleshoot-update-display ()
  (switch-to-buffer emacsism/troubleshoot-buffer-name)
  (erase-buffer)
  (widget-insert "Troubleshooting Information\n"
                 "===========================\n"
                 "\n"
                 "Version\n"
                 "-------\n"
                 "Current: " emacsism-version "\n"
                 "Latest: --> Update check not yet supported <--\n"
                 "\n"
                 "Configuration\n"
                 "-------------\n"
                 "Home:      " (emacsism/find-home) "\n"
                 "Workspace: " (alist-get :workspace emacsism-config) "\n"
                 "Config:    " (alist-get :config emacsism-config) "\n"
                 "API key:   " (emacsism/mangle-token (alist-get :token emacsism-config)) "\n"
                 "\n"
                 "API Reachability\n"
                 "----------------\n"
                 "\n"
                 "GitHub:\n"
                 "    * https://api.github.com\n"
                 "    * " (or (alist-get :status emacsism/troubleshoot-github-info) "unknown") "\n"
                 "    * " (or (alist-get :timing emacsism/troubleshoot-github-info) "unknown") "\n"
                 "\n"
                 "Exercism:\n"
                 "    * " (alist-get :base-url emacsism-config) "\n"
                 "    * " (or (alist-get :status emacsism/troubleshoot-exercism-info) "unknown") "\n"
                 "    * " (or (alist-get :timing emacsism/troubleshoot-exercism-info) "unknown") "\n"))

(provide 'emacsism-troubleshoot)
;;; emacsism-troubleshoot ends here
