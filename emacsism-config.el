;;; emacsism-config --- Manages configuration of the emacsism package

;;; Commentary:

;;; Code:

(require 'f)
(require 'json)

(defvar emacsism-config '()
  "The configuration as read from the exercism configuration files.")

;;;###autoload
(defun emacsism/read-config (&optional dir)
  "Read the files `api.json' and `user.json' from DIR and populates the config variables."
  (let* ((dir (or dir (emacsism/find-config-dir)))
         (user-file (f-join dir "user.json"))
         (user-conf (json-read-file user-file))
         (base-url (alist-get 'apibaseurl user-conf))
         (workspace (alist-get 'workspace user-conf))
         (api-key (alist-get 'token user-conf))
         (conf `((:base-url . ,base-url)
                 (:workspace . ,workspace)
                 (:config . ,dir)
                 (:token . ,api-key))))
    (setq-default emacsism-config conf)
    conf))

(defun emacsism/find-config-dir ()
  "Find the directory that should contain the configuration."
  (let ((ex-conf-home (emacsism/string-or-nil (getenv "EXERCISM_CONFIG_HOME")))
        (xdg-conf-home (emacsism/string-or-nil (getenv "XDG_CONFIG_HOME"))))
    (or ex-conf-home xdg-conf-home (f-join (emacsism/find-home) ".config" "exercism"))))

(defun emacsism/string-or-nil (string)
  "Return either the given STRING or nil if it was nil or empty."
  (if (and (stringp string) (string= "" string))
      nil
    string))

(defun emacsism/find-home ()
  "Find the users home-folder."
  (getenv "HOME"))

(provide 'emacsism-config)
;;; emacsism-config.el ends here
