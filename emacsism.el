;;; emacsism --- An emacs implementation of the exercism client

;;; Commentary:

;;; Code:

(require 'emacsism-config)
(require 'emacsism-troubleshoot)
(require 'request)
(require 's)

;;;###autoload
(defconst emacsism-version "0.0.1-pre")

(defun emacsism/operating-system ()
  (pcase system-type
    ('gnu/linux "Linux")))

(defun emacsism/mangle-token (token)
  (let* ((rep (- (length token) 8))
         (head (s-left 4 token))
         (tail (s-right 4 token))
         (mid (s-repeat rep "*")))
    (s-concat head mid tail)))

(defun emacsism/ping (conf)
  (let* ((url (s-concat (alist-get :base-url conf) "/ping")))
    (request url
             :parser 'json-read)))

(provide 'emacsism)
;;; emacsism.el ends here
